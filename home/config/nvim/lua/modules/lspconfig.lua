local M = {
  "neovim/nvim-lspconfig",
  event = "LazyFile"
}

local servers = {
  clangd = {
    capabilities = { offsetEncoding = "utf-8" }
  },
  biome = {},
  cssls = {},
  emmet_language_server = {},
  html = {},
  jdtls = {},
  jsonls = {},
  lua_ls = {
    settings = {
      Lua = {
        runtime = {
          version = "LuaJIT"
        },
        diagnostics = {
          globals = { "vim" },
        },
        workspace = {
          library = vim.api.nvim_get_runtime_file("", true),
          checkThirdParty = false
        },
        telemetry = {
          enable = false
        },
        format = {
          enable = true,
          defaultConfig = {
            indent_style = "space",
            indent_size = "2"
          }
        }
      }
    }
  },
  marksman = {},
  omnisharp = {
    cmd = { "omnisharp" }
  },
  rust_analyzer = {},
  tailwindcss = {
    filetypes = { "rust", "aspnetcorerazor", "astro", "astro-markdown", "blade", "clojure", "django-html", "htmldjango",
      "edge", "eelixir", "elixir", "ejs", "erb", "eruby", "gohtml", "gohtmltmpl", "haml", "handlebars", "hbs", "html",
      "html-eex", "heex", "jade", "leaf", "liquid", "markdown", "mdx", "mustache", "njk", "nunjucks", "php", "razor",
      "slim", "twig", "css", "less", "postcss", "sass", "scss", "stylus", "sugarss", "javascript", "javascriptreact",
      "reason", "rescript", "typescript", "typescriptreact", "vue", "svelte" },
    init_options = {
      userLanguages = {
        rust = "html"
      }
    },
  },
  taplo = {},
  tsserver = {
    init_options = {
      preferences = {
        includeInlayParameterNameHints = 'all',
        includeInlayParameterNameHintsWhenArgumentMatchesName = true,
        includeInlayFunctionParameterTypeHints = true,
        includeInlayVariableTypeHints = true,
        includeInlayPropertyDeclarationTypeHints = true,
        includeInlayFunctionLikeReturnTypeHints = true,
        includeInlayEnumMemberValueHints = true,
      }
    }
  },
  zls = {}
}

M.config = function()
  vim.lsp.handlers['textDocument/hover'] = vim.lsp.with(
    vim.lsp.handlers.hover,
    { border = 'rounded' }
  )

  vim.lsp.handlers['textDocument/signatureHelp'] = vim.lsp.with(
    vim.lsp.handlers.signature_help,
    { border = 'rounded' }
  )

  vim.diagnostic.config({
    virtual_text = true,
    signs = true,
    update_in_insert = true,
    underline = true,
    severity_sort = true
  })

  local lspconfig = require("lspconfig")

  for server, opts in pairs(servers) do
    lspconfig[server].setup(opts)
  end
end

return M
