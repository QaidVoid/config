return {
  {
    "ibhagwan/fzf-lua",
    cmd = "FzfLua",
    keys = {
      { "<leader>pf",      ":FzfLua files<CR>",                 desc = "Project files" },
      { "<leader>pg",      ":FzfLua live_grep<CR>",             desc = "Live Grep" },
      { "<leader>pG",      ":FzfLua live_grep_glob<CR>",        desc = "Grep with glob" },
      { "<leader>pd",      ":FzfLua diagnostics_workspace<CR>", desc = "Project diagnostics" },
      { "<leader>pD",      ":FzfLua diagnostics_document<CR>",  desc = "Document diagnostics" },

      { "<leader>gf",      ":FzfLua git_files<CR>",             desc = "Git files" },
      { "<leader>gs",      ":FzfLua git_status<CR>",            desc = "Git status" },
      { "<leader>gc",      ":FzfLua git_commits<CR>",           desc = "Git commits of project" },
      { "<leader>gb",      ":FzfLua git_bcommits<CR>",          desc = "Git commits of buffer" },
      { "<leader>gt",      ":FzfLua git_tags<CR>",              desc = "Git tags" },
      { "<leader>gS",      ":FzfLua git_stash<CR>",             desc = "Git stash" },

      { "<leader><space>", ":FzfLua buffers<CR>",               desc = "Open buffers" },
    },
    opts = function()
      return {
        "fzf-native",
        fzf_bin = "sk",
        git = {
          status = {
            preview_pager = "delta --width=$FZF_PREVIEW_COLUMNS"
          }
        }
      }
    end,
  },

  {
    "brenoprata10/nvim-highlight-colors",
    event = "LazyFile",
    opts = {
      render = "background",
      enable_named_colors = true,
      enable_tailwind = true
    }
  },

  {
    "folke/todo-comments.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim"
    },
    event = "LazyFile",
    config = true,
    keys = {
      { "]t", function() require("todo-comments").jump_next() end, desc = "Next todo comment" },
      { "[t", function() require("todo-comments").jump_prev() end, desc = "Previous todo comment" },
    },
  },

  {
    "stevearc/oil.nvim",
    keys = {
      { "-", ":Oil<CR>", desc = "Open parent directory" }
    },
    opts = {}
  },

  {
    "numToStr/Comment.nvim",
    event = "LazyFile",
    opts = {}
  },

  {
    "lewis6991/gitsigns.nvim",
    event = "LazyFile",
    opts = {
      signs = {
        add = { text = "▎" },
        change = { text = "▎" },
        delete = { text = "" },
        topdelete = { text = "" },
        changedelete = { text = "▎" },
        untracked = { text = "▎" },
      },
      current_line_blame = true,
      current_line_blame_opts = {
        virt_text = true,
        virt_text_pos = 'eol',
        delay = 300
      },
      on_attach = function(buffer)
        local gs = package.loaded.gitsigns

        local function map(mode, l, r, desc)
          vim.keymap.set(mode, l, r, { buffer = buffer, desc = desc })
        end

        map("n", "]h", gs.next_hunk, "Next Hunk")
        map("n", "[h", gs.prev_hunk, "Prev Hunk")
        map({ "n", "v" }, "<leader>ghs", ":Gitsigns stage_hunk<CR>", "Stage Hunk")
        map({ "n", "v" }, "<leader>ghr", ":Gitsigns reset_hunk<CR>", "Reset Hunk")
        map("n", "<leader>ghS", gs.stage_buffer, "Stage Buffer")
        map("n", "<leader>ghu", gs.undo_stage_hunk, "Undo Stage Hunk")
        map("n", "<leader>ghR", gs.reset_buffer, "Reset Buffer")
        map("n", "<leader>ghp", gs.preview_hunk, "Preview Hunk")
        map("n", "<leader>ghb", function() gs.blame_line({ full = true }) end, "Blame Line")
        map("n", "<leader>ghd", gs.diffthis, "Diff This")
        map("n", "<leader>ghD", function() gs.diffthis("~") end, "Diff This ~")
        map({ "o", "x" }, "ih", ":<C-U>Gitsigns select_hunk<CR>", "GitSigns Select Hunk")
      end,
    },
  }
}
