local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "--single-branch",
    "https://github.com/folke/lazy.nvim.git",
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local opts = {
  -- defaults = {
  --   lazy = true
  -- },
  ui = {
    border = "rounded",
  },
  install = {
    colorscheme = {
      "catppuccin",
    },
  },
  performance = {
    rtp = {
      disabled_plugins = {
        "fzf",
        "gzip",
        "matchit",
        "netrwPlugin",
        "rplugin",
        "spellfile",
        "tarPlugin",
        "tohtml",
        "tutor",
        "zipPlugin"
      }
    },
    cache = {
      enabled = true
    }
  },
  checker = {
    enabled = false
  },
}

require("utils").lazy_file()
require("lazy").setup("modules", opts)
