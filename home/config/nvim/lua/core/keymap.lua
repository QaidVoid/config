local utils = require("utils")
local keymap = utils.keymap

-- Switch buffers
keymap("n", "<leader>bn", ":bn<CR>", { desc = "Go to next buffer" })
keymap("n", "<leader>bp", ":bp<CR>", { desc = "Go to previous buffer" })

-- Replace text without yanking
keymap("x", "p", '"_dP')

-- Move lines
keymap("v", "K", ":m '<-2<CR>gv=gv")
keymap("v", "J", ":m '>+1<CR>gv=gv")
keymap("n", "J", "mzJ`z")

-- Visual mode indent
keymap("v", "<", "<gv")
keymap("v", ">", ">gv")

-- Visual guides
keymap("n", "<C-d>", "<C-d>zz")
keymap("n", "<C-u>", "<C-u>zz")
keymap("n", "n", "nzzzv")
keymap("n", "N", "Nzzzv")

keymap("n", "Q", "<nop>")
