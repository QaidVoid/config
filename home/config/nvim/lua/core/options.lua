vim.g.mapleader   = ' '

local opt         = vim.o

opt.backup        = false
opt.writebackup   = false

opt.undodir       = vim.fn.stdpath('data') .. '/undodir'
opt.undofile      = true

opt.breakindent   = true
opt.clipboard     = "unnamedplus"
opt.cursorline    = true
opt.laststatus    = 3
opt.list          = true
opt.number        = true
opt.pumblend      = 10
opt.pumheight     = 10
opt.pumwidth      = 20
opt.scrolloff     = 8
opt.shortmess     = 'aoOWFcC'
opt.showmode      = false
opt.signcolumn    = "yes"
opt.splitbelow    = true
opt.splitright    = true
opt.termguicolors = true
opt.winblend      = 10
opt.wrap          = false

opt.autoindent    = true
opt.completeopt   = "menuone,noinsert,noselect"
opt.expandtab     = true
opt.formatoptions = 'jcroqlnt'
opt.ignorecase    = true
opt.inccommand    = "nosplit"
opt.incsearch     = true
opt.infercase     = true
opt.shiftwidth    = 2
opt.smartcase     = true
opt.smartindent   = true
opt.tabstop       = 2

opt.foldenable    = false
