vim.api.nvim_create_autocmd("LspAttach", {
  desc = "LSP attach",
  callback = function(args)
    local bufnr = args.buf
    local client = vim.lsp.get_client_by_id(args.data.client_id)

    assert(client)

    if client.server_capabilities.documentHighlightProvider then
      vim.api.nvim_create_augroup("lsp_document_highlight", {
        clear = true
      })

      vim.api.nvim_clear_autocmds({
        buffer = bufnr,
        group = "lsp_document_highlight"
      })

      vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
        buffer = bufnr,
        callback = vim.lsp.buf.document_highlight,
        group = "lsp_document_highlight"
      })
      vim.api.nvim_create_autocmd({ "CursorMoved", "CursorMovedI" }, {
        buffer = bufnr,
        callback = vim.lsp.buf.clear_references,
        group = "lsp_document_highlight"
      })
    end

    if client.server_capabilities.inlayHintProvider then
      vim.lsp.inlay_hint.enable(bufnr)
    end

    if client.name == "tsserver" then
      client.server_capabilities.documentFormattingProvider = false
    end

    local keymap = require("utils").keymapbuf
    local fzf = require("fzf-lua")

    keymap("n", "K", vim.lsp.buf.hover, { desc = "LSP Hover" })
    keymap("n", "<C-k>", vim.lsp.buf.signature_help, { desc = "LSP Signature Help" })
    keymap("n", "<leader>rn", vim.lsp.buf.rename, { desc = "Rename symbol" })
    keymap("n", "[d", function() vim.diagnostic.goto_prev({ border = "rounded" }) end,
      { desc = "Go to previous diagnostic" })
    keymap("n", "]d", function() vim.diagnostic.goto_next({ border = "rounded" }) end,
      { desc = "Go to next diagnostic" })
    keymap("n", "gd", fzf.lsp_definitions, { desc = "LSP Definitions" })
    keymap("n", "gD", fzf.lsp_declarations, { desc = "LSP Declarations" })
    keymap("n", "<leader>ca", fzf.lsp_code_actions, { desc = "Code Actions" })
    keymap("n", "<leader>gr", fzf.lsp_references, { desc = "LSP References" })
    keymap("n", "<leader>gi", fzf.lsp_implementations, { desc = "LSP Implementations" })
    keymap("n", "<leader>lr", ":LspRestart<CR>", { desc = "Restart LSP" })
    keymap("n", "<leader>ih",
      function() vim.lsp.inlay_hint.enable(bufnr, not vim.lsp.inlay_hint.get({ bufnr = 0 })[1]) end,
      { desc = "Toggle Inlay Hint" })
    keymap("n", "<leader>fd", vim.lsp.buf.format, { desc = "Format Document" })
  end
})
