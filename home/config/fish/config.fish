set fish_greeting

fish_add_path $HOME/.local/bin $HOME/.cargo/bin
set -U MOZ_ENABLE_WAYLAND 1
set -x GPG_TTY (tty)

if test -z "$XDG_RUNTIME_DIR"
  set -gx XDG_RUNTIME_DIR /tmp/(id -u)-runtime-dir
  if ! test -d "$XDG_RUNTIME_DIR"
    mkdir "$XDG_RUNTIME_DIR"
    chmod 0700 "$XDG_RUNTIME_DIR"
  end
end

if type -q nvim
  set -U EDITOR nvim
  alias vi=nvim
end

if type -q pnpm
  alias npm=pnpm
  set -gx PNPM_HOME $HOME/.local/share/pnpm
  fish_add_path $PNPM_HOME
end

alias ls=eza
alias ll="ls -lh --icons"
alias la="ll -a"
alias cat=bat

bind \cf fzf_projects

update_cwd_osc
