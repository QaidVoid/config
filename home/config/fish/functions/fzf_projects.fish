function fzf_projects
    set selection (fd . $HOME/dev -d 2 | fzf --preview 'eza -1 --icons {}' --preview-window up:30%:wrap)
    if test -n "$selection"
        cd "$selection"
        commandline -f repaint
    end
end
