function fish_prompt
  set -l normal (set_color normal)
  set -l cwd_color (set_color $fish_color_cwd)
  set -l red (set_color red)
  set -l blue (set_color blue)
  set -l yellow (set_color yellow)

  if functions -q fish_is_root_user; and fish_is_root_user
    set_color -o red
  else
    set_color -o yellow
  end

  if test -z "$SSH_CLIENT"
    set_color -o blue
  else
    set_color -o cyan
  end

  echo 

  #[PWD]
  echo -ns $normal '[' $cwd_color (prompt_pwd) $normal ']' $normal

  # [GIT]
  set -g __fish_git_prompt_use_informative_chars true
  set -g __fish_git_prompt_showupstream verbose name
  set -g __fish_git_prompt_showcolorhints true
  set -g __fish_git_prompt_char_stateseparator ' '
  set -g __fish_git_prompt_color_branch_dirty $red
  set -g __fish_git_prompt_color_branch_staged $yellow

  set -l git_prompt (fish_git_prompt '%s')
  test -n "$git_prompt"
  and echo -ns $normal '[' $red ' ' $git_prompt ']'

  echo ''

  # [USER]
  echo -s $normal '[' $blue $USER $normal '] '
end
