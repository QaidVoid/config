config.load_autoconfig(False)
config.source('tokyonight.py')
c.auto_save.session = True

# Dark mode
c.colors.webpage.preferred_color_scheme = 'dark'
c.colors.webpage.darkmode.enabled = True
c.colors.webpage.darkmode.grayscale.images = 0.25

c.tabs.mode_on_change = "restore"
c.tabs.position = "left"

# Breaks hint ;(
# c.qt.chromium.process_model = "process-per-site"

# Disallow 3rd party cookies
c.content.cookies.accept = 'no-3rdparty'

c.editor.command = ["foot", "nvim", "{file}", "-c", "normal {line}G{column0}l"]

config.bind('tt', 'config-cycle tabs.show never always', 'normal')

# Let M$ steal my cookies
config.set("content.cookies.accept", "all", "https://teams.microsoft.com/")
